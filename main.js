/**
 *  See goodbye to some douban groups.
 *  @author D.
 * 
 *  @usage:
 *      1. login and open "http://www.douban.com/group/people/{your name here}/joins"
 *      2. open the console. run this script once then call it's "init" method and you'll see the interface has changed, it's that easy~ 
 */

window.SayGoodBye = window.SayGoodBye || (function(win, undefined){
    /**
     *  To extract templates from function comments.
     *  @param {Function}   template provider function
     */
    function _extractTpl( func ) {
        try {
            return func.toString().replace(/(^[\s\S]*\/\*\s*)|(\s*\*\/[\s\S]*)/g, "");
        } catch (e) {
            console.log("Failed resolving template from given function, please check.");
            console.log(e);
        }
    }

    var _strActions = _extractTpl(function(){/*
        <ul class="group-actions clearfix">
            <li class="action-backup"><a href="javascript:;">Backup Group List</a></li>
            <li class="action-select"><a href="javascript:;">Select All Groups</a></li>
            <li class="action-saygoodbye"><a href="javascript:;">Say Goodbye</a></li>
        </ul>
    */});

    var _strListPreview = _extractTpl(function(){/*
        <p class="list-preview"></p>
    */});

    var _strWrapper = _extractTpl(function(){/*  
        <div class="checkbox-wrapper">
            <input type="checkbox" />
            <label></label>
        </div>
    */});


    var _strCss = _extractTpl(function(){/*
        .group-actions {
            margin-bottom: 20px;
        }
        .group-actions li {
            float: left;
            height: 26px;
            margin-righ: 10px;
        }
        .group-actions li a {
            display: block;
            padding: 0 10px;
            line-height: 26px;
            color: #FF0000;
        }
        .group-actions li a:hover {
            color: #FFFFFF;
        }

        .list-preview strong {
            font-weight: bold;
        }

        .group-list li {
            position: relative;
        }
        .group-list li .checkbox-wrapper {
            position: absolute;
            right: 0px;
            top: 0px;
            width: 32px;
            height: 32px;
            background: #FFFFFF;
        }
        .group-list li .checkbox-wrapper input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            width: 100%;
            height: 100%;
        }
        .group-list li .checkbox-wrapper label {
            display: block;
            width: 100%;
            height: 100%;
            background:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAABKCAMAAADty6b6AAABSlBMVEUAAADXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBXXaBVr3ykbAAAAbXRSTlMABRMUCFLL/tc0hNkZv+RDTJgvJQ0Sz/ZFnxHONvM+EMyGKOkjD8roKg7IKT8gx+dE4cY19yYMxHnD5icX2+o5whrlAT0Y1pADFtQdBBXRJMnj8UJLQPW4qqi9qUFGdXi6uwZUVogul0+dO/S8CcfLrwAAAW5JREFUeF7tlEdTIlEURq+N0irSqCCNYkBMmMOMOWF2ZjDBoOJgwhy+/7/10Ym+1fSws4oqzvKds7ib99F3USd5GPVcN3hlcNDYZPPNcOJrIcmvBDTfCrS1ezjBEHWEATVCAj86yUlXFAK1W4ge9Dp9Xwwa/UIAcaf36n5gsHwwNKz7kQSVDUbHDD9OpWBCCU+afkr308JbwYwKzA5qPjTHvB78+AmBMi/8wqLh42QLlozHBC0s635llezB2jo0NjaTlmcBbflgZ3uednZZQHv73NMBDllAv34z/wdI8YCOjk0fFOLEGZAU1rwSJJeAOmJFf0KuAfWd4kx4PbCOTCNDJvV/JaPN4pwMLnBJTnK4MoOMjH/5OCefA66t+AbluKUSgbs0OOn7CP2HGjUKfGulAtcPMRkcOfpYYWuBJxK4bu3zC/BKAvetfUNSs25bS+9Q2YQw+O90D1LVEtQCvrVOsviotLWflbY2UjVb+wU5zOMJOr3zlwAAAABJRU5ErkJggg==") -2px -42px;
        }
        .group-list li .checkbox-wrapper input:checked + label {
            background-position: 0 0;
        }
    */});

    /**
     *  Generate a combined preview string from the current chosen list.
     */
    function _generateListPreviewStr( previewList ) {
        var _str = "<strong>" + (Object.keys(previewList).length === 1 ? "This guy" : "These guys") + " will be sent back to Mars:</strong> ";

        for ( group in previewList ) {
            _str += previewList[group].name + ", ";
        }

        return _str.slice(0, -2) + ".";
    }

    // list preview
    var _listPreview = {};

    // original list
    // to backup the original list.
    var _originalList = {};

    /**
     *  Initialize the program.
     */
    function _init(){
        var styleEl = document.createElement("style");

        styleEl.setAttribute("type", "text/css");
        styleEl.innerHTML = _strCss;
        document.getElementsByTagName("head")[0].appendChild(styleEl);

        $(".group-list li").append(_strWrapper);

        $(".group-list").before(_strActions);

        $(".group-list").before(_strListPreview);


        /**
         *  Get a copy of _originalList for _listPreview, and set gids.
         */
        $(".group-list li").each(function(idx, el){
            var gid = $(this).find(".pic a").attr("href").split("/").slice(-2, -1).pop(),

                name = $(this).find(".pic a img").attr("alt"),

                image = $(this).find(".pic a img").attr("src");

            $(this).find(".checkbox-wrapper input").attr("gid", gid);

            _originalList[gid] = {
                name: name,
                image: image
            };
        });

        /**
         *  Select/Unselect all group items.
         */
        $(".group-actions").find(".action-select").bind("click", function(){
            var allInputs = $(".group-list li .checkbox-wrapper input"),

                selected = $(this).hasClass("selected");


            allInputs.each(function(idx, el){
                var gid = $(el).attr("gid");

                if ( selected ) {
                    el.checked = false;

                    delete _listPreview[ gid ];
                } else {
                    el.checked = true;

                    if ( !_listPreview[ gid ] ){
                        _listPreview[ gid ] = $.extend({}, _originalList[ gid ]);
                    }
                }
            });

            if ( selected ) {
                $(this).removeClass("selected");

                $(this).find("a").html("Select All Groups");

                $(".list-preview").html("");
            } else {
                $(this).addClass("selected");

                $(this).find("a").html("Unselect All Groups");

                $(".list-preview").html(_generateListPreviewStr(_listPreview));
            }
        });

        
        /**
         *  Select/Unselect single group.
         */
        $(".group-list li .checkbox-wrapper input").bind("click", function(){
            var gid = $(this).attr("gid");
                
            if ( this.checked === true ) {
                if ( !_listPreview[ gid ] ) {
                    _listPreview[ gid ] = $.extend({}, _originalList[ gid ]);
                }
            } else {
                delete _listPreview[ gid ];
            }

            if ( Object.keys(_listPreview).length > 0 ) {
                $(".list-preview").html(_generateListPreviewStr(_listPreview));
            } else {
                $(".list-preview").html("");
            }
        });


        /**
         *  Backup group list
         */
        $(".group-actions").find(".action-backup").bind("click", function(){
            console.log("Save this json to a reliable place, later there will be a feature to batch join groups from this list.");
            console.log(JSON.strinify(_originalList));
        });


        /**
         *  Say goodbye to chosen groups.
         */
        $(".group-actions").find(".action-saygoodbye").bind("click", function(){
            if ( !confirm("Are you sure you want to say goodbye to those guys?(Anyway, you can add them back later)") ) {
                return false;
            }

            for ( gid in _listPreview ) {
                console.log("saying goodbye to " + _listPreview[gid].name);

                _sayGoodbyeTo( gid, (function(gid){
                    return function(){
                        console.log( _listPreview[gid].name + " has already gone home.");

                        delete _listPreview[gid];

                        $(".group-list").find("li .checkbox-wrapper input[gid=" + gid + "]").parents("li").fadeOut(function(){
                            $(this).remove();
                        });
                    };
                })(gid) );
            }
        });


        /**
         *  Actual function to say goodbye to one group.
         *  @param {String} gid     group id
         *  @param {Function|optional}  callback
         */
        function _sayGoodbyeTo( gid, then ) {
            // TODO, remove element from DOM tree.
            $.ajax({
                type: "GET",
                url: "/group/" + gid + "/?action=quit&ck=" + get_cookie("ck"),
                data: null,
                success: then || function(){
                    console.log( gid + " has already gone home.");
                }
            });
        }
    }

    // expose the APIs
    return {
        init: _init,

        chosenList: _listPreview
    };
})(window, undefined);

SayGoodBye.init();
