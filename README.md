# README #

### What is this repository for? ###

* Douban doesn't provide you a way for batch removing those groups you are not interested in any more, this script wants to provide you a way for that.


### How to use it? ###

* In theory, you don't need to be a skillful front-end developer to use this script, coz it's really easy :). But if you know something behind this script when reading the code, it'll be better since you can customize it to meet your needs.

* So, I suppose you know nothing about the code but you want to use it, follow these steps and you'll be good.

    * Use a browser(when I say "browser", I mean "firefox" or "chrome" or something else, what? you are using IE? Well, grandma, I can't help you then~)

    * Login into douban.

    * Open the page displaying your joined groups: http://www.douban.com/group/people/{your douban account name}/joins; it's ok if you don't know your account name, first go to http://www.douban.com/group/, then click "我的小组主页", after the page jumped, click "加入的小组" and you'll be there.

    * Open the console, often, you double click on the page and you'll see a context menu like "Inspect Element", click on it and there will be a panel appeared, then you choose "Console", till you see one input box at the bottom of the panel with a ">>" symbol preceeding it.

    * Then you paste the whole code from "main.js" into the input box, and press "Enter" key, if everything is ok, you'll see the page layout has changed, some extra buttons and checkboxes are inserted into the page to let you deal with the groups.

    * Then you choose some groups, and click "Say Goodbye" button and wait for a while, you'll se those groups have already gone back to Mars :).


### TODOs ###

* I'm thinking of backing up the original list and publishing it as a private note, so that later if you want all the groups back, you'll know who they exactly are.

* Implement a feature to add all the groups back from a backup list.
